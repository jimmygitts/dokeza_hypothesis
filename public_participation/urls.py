from __future__ import unicode_literals
from django.conf.urls import url

from .views import PublicEventsList, PublicEventView, ParticipationList, ParticipationHelpView
from posts.views import MemorandumListView, MemorandumDisplayView, PetitionListView, PetitionDisplayView

urlpatterns = [
    url(r'^$', ParticipationList.as_view(), name='home'),
    url(r'^events/$', PublicEventsList.as_view(), name='events'),
    url(r'^events/(?P<slug>[\w-]+)/$', PublicEventView.as_view(), name='event'),
    url(r'^memoranda/$', MemorandumListView.as_view(), name='memoranda'),
    url(r'^memoranda/(?P<slug>[\w-]+)/$', MemorandumDisplayView.as_view(), name='memorandum'),
    url(r'^petitions/$', PetitionListView.as_view(), name='petitions'),
    url(r'^petitions/(?P<slug>[\w-]+)/$', PetitionDisplayView.as_view(), name='petition-detail'),
    url(r'^help/$', ParticipationHelpView.as_view(), name='help'),
]
