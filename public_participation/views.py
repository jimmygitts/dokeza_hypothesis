import time
from datetime import datetime, timedelta
import calendar

from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView, View

from .models import PublicEvent
from posts.models import Memorandum, Petition

mnames = "January February March April May June July August September October November December"
mnames = mnames.split()


class ParticipationList(TemplateView):
    model = PublicEvent
    template_name = 'public_participation/public_list.html'

    def get_context_data(self, **kwargs):
        context = super(ParticipationList, self).get_context_data(**kwargs)
        context['events'] = PublicEvent.objects.all()
        context['memoranda'] = Memorandum.objects.all()
        context['petitions'] = Petition.objects.all()
        context['page'] = 'public'
        context['stingo'] = 'all'
        return context


class PublicEventsList(ListView):
    model = PublicEvent
    template_name = 'public_participation/public_events.html'

    def get_context_data(self, **kwargs):
        context = super(PublicEventsList, self).get_context_data(**kwargs)
        public_events = PublicEvent.objects.all()
        """Listing of days in `month`."""
        year = time.localtime()[0]
        month = time.localtime()[1]
        day = time.localtime()[2]
        lst = []

        # apply next / previous change
        change = ()
        if change in ("next", "prev"):
            now, mdelta = datetime(year, month, 1), timedelta(days=31)
            if change == "next":
                mod = mdelta
            elif change == "prev":
                mod = -mdelta

            year, month = (now + mod).timetuple()[:2]

        # init variables
        cal = calendar.Calendar()
        month_days = cal.itermonthdays(year, month)
        nyear, nmonth, nday = time.localtime()[:3]
        lst = [[]]
        week = 0

        # make month lists containing list of days for each week
        # each day tuple will contain list of events and 'current' indicator
        for day in month_days:
            events = current = False   # are there events for this day; current day?
            if day:
                events = PublicEvent.objects.filter(start__year=year, start__month=month, start__day=day)
                if day == nday and year == nyear and month == nmonth:
                    current = True

            lst[week].append((day, events, current))
            if len(lst[week]) == 7:
                lst.append([])
                week += 1

        context = {
            'events': public_events,
            'year': year,
            'month': month,
            'month_days': lst,
            'mname': mnames[month - 1],
            'stingo': 'events',
            'page': 'public'
        }
        return context


class PublicListView(View):

    def get_events(self, request, *args, **kwargs):
        view = PublicEventsList.as_view()
        return view(request, *args, **kwargs)


class PublicEventView(TemplateView):
    model = PublicEvent
    template_name = 'public_participation/public_events.html'

    def get_context_data(self, *args, **kwargs):
        context = super(PublicEventView, self).get_context_data(**kwargs)
        context["page"] = "public"
        context["stingo"] = "events"
        return context


class ParticipationHelpView(TemplateView):
    template_name = 'public_participation/help.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ParticipationHelpView, self).get_context_data(**kwargs)
        context["page"] = "help"
        context["stingo"] = "faqs"
        return context
