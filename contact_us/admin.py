from django.contrib import admin

from .models import Contact

class ContactAdmin(admin.ModelAdmin):

    list_display = ('first_name', 'last_name', 'email_address')
    fieldsets = (
        (None, {
            'fields': [
                ('first_name', 'last_name'),
                ('email_address',),
                ('company', 'county'),
                ('phone_number'),
                ('message'),
            ]
        }),
    )


admin.site.register(Contact, ContactAdmin)
