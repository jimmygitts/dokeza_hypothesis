#!/usr/bin/env python
# encoding: utf-8

"""
Created by Jimmy Gitonga on 2016-10-27.
Copyright (c) 2016 Afroshok. All rights reserved.
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink


class Contact(models.Model):
    """Contact model."""
    first_name = models.CharField(_('First Name'), max_length=50)
    last_name = models.CharField(_('Last Name'), max_length=50)
    email_address = models.EmailField(_('Email Address'))
    company = models.CharField(_('Company'), max_length=100, blank=True)
    county = models.CharField(_('County'), max_length=100, blank=True)
    phone_number = models.IntegerField(_('Phone Number'), null=True, blank=True)
    message = models.TextField(_('Message'), max_length=1000)

    class Meta:
        verbose_name = _('Contact Enquiry')
        verbose_name_plural = _('Contact Enquiries')

    def __str__(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()
