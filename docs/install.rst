Install
=========

This project has been created using the Docker Toolbox on the macOS. This works as well on the Docker Toolbox in any Linux choice.

However, one can set up the **Docker Engine** using the application best for the platform. For the install the Docker Engine, go to `Install Docker Engine`_.

Clone the repo and go to the project's root folder on the CLI.
List the Docker machines present::

    $ docker-machine ls

Start up the default Docker machine and go into it::

    $ docker-machine start default
    $ eval $(docker-machine env default)

When using default machine, no database or its data is saved once the machine is turned off or the computer being used is restarted. This might be useful if the design of the database is not complete. Once the database schema is settled, a specific machine can be created.

To create a specific machine for development, type::
    
    $ docker-machine create --driver virtualbox --help

This will give print out the options to use when creating the machine. Then type::

    $ docker-machine create --driver virtualbox dev-machine
    $ eval $(docker-machine env dev-machine)

To confirm you are in the root folder of the project, by listing the files, you should see ``docker-compose.yml`` and ``dev.yml``.

For Development
---------------
The ``dev.yml`` is the file that controls your Docker development stack. The ``-f`` attribute informs ``docker-compose`` that it should use the file ``dev.yml``.

To build and fire up docker for development, you should type::

    $ docker-compose -f dev.yml build

and after some amount of time::

    $ docker-compose -f dev.yml up -d

To see what the URL of the machine to use, type::

    $ docker-machine ls

On your browser, navigate to the URL http://192.168.99.100:8000 for the ``default`` docker machine or the URL found related to the ``dev-machine`` when you list the docker machines on the CLI.

For Deployment
--------------



Install PostgeSQL and PostGIS
-----------------------------

Navigate to the Docker container that has the PostgreSQL database::

    $ docker exec -it <postgres_container_id> bash
    root@<postgres_container_id>:/# apt-get update
    root@<postgres_container_id>:/# apt-get install postgresql-9.x-postgis

This will take some time. The download is over 400MB. Finally, enable GIS for the database::

    root@<postgres_container_id>:/# psql -U [yourdatabase] -c "CREATE EXTENSION postgis;"

The command will be echoed on the CLI, ```CREATE EXTENSION```. You can exit after that.


Open your text editor and code some magic.

.. _Install Docker Engine: https://docs.docker.com/engine/installation/