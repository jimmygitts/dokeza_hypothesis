# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views

from rest_framework_jwt.views import obtain_jwt_token
from dokeza.users.api.views import get_jwt_token
from .views import HomeView, AboutView, HelpView, ContactView, CalendarView, Root, TagIndexView

from annotation import views

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^about/$', AboutView.as_view(), name='about'),
    url(r'^help/$', HelpView.as_view(), name='help'),
    url(r'^news/', include('posts.urls', namespace='posts')),
    url(r'^contacts/$', ContactView.as_view(), name='contacts'),
    url(r'^bills/', include('bills.urls', namespace='bills')),
    url(r'^public-participation/', include('public_participation.urls', namespace='public_participation')),

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),

    # User management
    url(r'^users/', include('dokeza.users.urls', namespace='users')),
    url(r'^accounts/', include('allauth.urls')),

    # These are the regular urls.

    # url(r'^annotations/', include('annotation.urls',
    #                               namespace='annotator')),
    url(r'^bills/', include('bills.urls', namespace='bills')),
    url(r'^comments/', include('comments.urls', namespace='comments')),
    url(r'^categories/$', TagIndexView.as_view(), name='categories'),

    url(r'^calendar/', include('cal.urls', namespace='kalenda')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # Annotation URLS
    url(r"^annon_root/$", views.root, name="root"),
    url(r"^annotations/(?P<pk>.+)/?$",
        views.read_update_delete, name="read_update_delete"),
    url(r"^annotations/?$", views.index_create, name="index_create"),
    url(r"^search/?$", views.search, name="search"),
    url(r"^demo/?$", views.DemoView.as_view(), name="demo"),

    # ------- These are the API urls ------------
    # Authentication APIs
    url(r'^api/$', Root),
    url(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/auth/token/$', get_jwt_token),
    url(r'^api/users/', include('dokeza.users.api.urls', namespace='users-api')),

    # Access APIs
    url(r'^api/bills/', include('bills.api.urls', namespace='bills-api')),
    url(r'^api/news/', include('posts.api.urls', namespace='posts-api')),
    url(r'^api/comments/', include('comments.api.urls', namespace='comments-api')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
