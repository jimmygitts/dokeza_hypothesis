from django.conf.urls import url

from .views import (
    PostListView,
    PostDetailView,
    MemorandumListView,
    MemorandumDisplayView
)

urlpatterns = [
    url(r'^$', PostListView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', PostDetailView.as_view(), name='detail'),
    url(r'^memoranda/$', MemorandumListView.as_view(), name='memo-list'),
    url(r'^memoranda/(?P<slug>[\w-]+)/$', MemorandumDisplayView.as_view(), name='memo-detail'),
]
