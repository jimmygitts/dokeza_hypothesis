from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.views.generic import View  # Changes in 1.10, removes generic
from django.views.generic import ListView, FormView, TemplateView, UpdateView
from django.views.generic.detail import SingleObjectMixin, SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView
# from taggit.models import Tag

from .models import Post, Memorandum, Petition
from comments.forms import CommentForm
from comments.models import Comment

from hitcount.views import HitCountDetailView

"""
We will be using Class Based Views to run these. Functions would be the
easiest and best for such light work, but these are no longer supported by
django.
"""


# class TagMixin(object):
#     def get_context_data(self, **kwargs):
#         context = super(TagMixin, self).get_context_data(**kwargs)
#         context['tags'] = Tag.objects.all()
#         return context


class PostListView(ListView):
    model = Post
    template_name = 'news/news_list.html'

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context["page"] = "home"
        context["stingo"] = "news"
        while Post.objects.all() is not None:
            try:
                if Post.objects.all().filter(draft=False) is None:
                    latest_post = "There are no published Posts."
                    context["latest_post"] = latest_post
                    return context
                else:
                    posts = Post.objects.all().filter(draft=False)
                    other_posts = posts[1:]
                    latest_post = posts[0]
                    context["posts"] = posts
                    context["other_posts"] = other_posts
                    context["latest_post"] = latest_post
                    return context
            except:
                return context


# class TagIndexView(TagMixin, ListView):
#     template_name = 'news/news_list.html'
#     model = Post
#     paginate_by = '10'
#     context_object_name = 'posts'

#     def get_queryset(self):
#         return Post.objects.filter(tags__slug=self.kwargs.get('slug'))


class PostDisplayView(HitCountDetailView):
    model = Post
    template_name = 'news/news_detail.html'
    count_hit = True

    def get_context_data(self, *args, **kwargs):
        context = super(PostDisplayView, self).get_context_data(**kwargs)
        comments = self.object.comments

        initial_data = {
            "content_type": self.object.get_content_type,
            "object_id": self.object.id,
        }
        context["comment_form"] = CommentForm(initial=initial_data)
        context["comments"] = comments
        context["page"] = "home"
        context["stingo"] = "news"
        return context


class PostCommentView(SingleObjectMixin, FormView):
    model = Post
    form_class = CommentForm
    template_name = 'posts/post_detail.html'

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()

        form = CommentForm(request.POST or None)
        if form.is_valid():
            c_type = form.cleaned_data.get("content_type")
            content_type = ContentType.objects.get(model=c_type)
            obj_id = form.cleaned_data.get('object_id')
            content_data = form.cleaned_data.get("content")
            parent_obj = None
            try:
                parent_id = int(request.POST.get("parent_id"))
            except:
                parent_id = None

            if parent_id:
                parent_qs = Comment.objects.filter(id=parent_id)
                if parent_qs.exists() and parent_qs.count() == 1:
                    parent_obj = parent_qs.first()

            new_comment, created = Comment.objects.get_or_create(
                user=request.user,
                content_type=content_type,
                object_id=obj_id,
                content=content_data,
                parent=parent_obj,
            )
            return HttpResponseRedirect(
                new_comment.content_object.get_absolute_url()
            )
        else:
            return self.form_invalid(form)


class PostDetailView(View):

    def get(self, request, *args, **kwargs):
        view = PostDisplayView.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = PostCommentView.as_view()
        return view(request, *args, **kwargs)


class MemorandumListView(ListView):
    model = Memorandum
    template_name = "public_participation/public_memoranda.html"

    def get_context_data(self, **kwargs):
        context = super(MemorandumListView, self).get_context_data(**kwargs)
        memoranda = Memorandum.objects.all()
        context["page"] = "public"
        context["stingo"] = "memoranda"
        context["memoranda"] = memoranda
        return context


class MemorandumDisplayView(HitCountDetailView):
    model = Memorandum
    count_hit = True

    def get_context_data(self, *args, **kwargs):
        context = super(MemorandumDisplayView, self).get_context_data(**kwargs)
        context["page"] = "public"
        context["stingo"] = "memoranda"
        return context


class PetitionListView(ListView):
    model = Petition
    template_name = "public_participation/public_petitions.html"

    def get_context_data(self, **kwargs):
        context = super(PetitionListView, self).get_context_data(**kwargs)
        petitions = Petition.objects.all()
        context["page"] = "public"
        context["stingo"] = "petitions"
        context["petitions"] = petitions
        return context


class PetitionDisplayView(HitCountDetailView):
    model = Petition
    count_hit = True

    def get_context_data(self, *args, **kwargs):
        context = super(PetitionDisplayView, self).get_context_data(**kwargs)
        context["page"] = "public"
        context["stingo"] = "petitions"
        return context


class PetitionDraftView(TemplateView):
    template_name = 'users/user_draftapetition.html'

    def get_context_data(self, **kwargs):
        context = super(PetitionDraftView, self).get_context_data(**kwargs)
        context = super(PetitionDraftView, self).get_context_data(**kwargs)
        context['page'] = 'users'
        context['stingo'] = 'draft_petition'
        return context

# class PetitionDraftView(SingleObjectTemplateResponseMixin, ModelFormMixin,
#         ProcessFormView):
    
#     def get_object(self, queryset=None):
#         try:
#             return super(PetitionDraftView,self).get_object(queryset)
#         except AttributeError:
#             return None

#     def get(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         return super(PetitionDraftView, self).get(request, *args, **kwargs)

#     def post(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         return super(PetitionDraftView, self).post(request, *args, **kwargs)

