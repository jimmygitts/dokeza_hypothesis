"""
An anonymous user should not be able to annotate a document.

A registered user should be able to annotate his or her document.

A verified user is a registered user whom the staff can give access
to a third party registered user's annotations.

The staff are able to take annotations in a particular document
and create a new document as memoranda to the document.
"""

from django.test import TestCase


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)
