Dokeza Annotator
================

On the 23rd of February 2017, the `W3C published`_ that Annotations on the Web have been accorded first class citizen status.
We had been developing with the ``annotoator.js`` library that has been officially abandoned on the 11th March 2017 at `annotateit.org`_.

We will continue to develop with the AnnotateIt library. We are leaving this here to be visited at a later date, in the possiblity that this 'Hypothes.is' might be used on the Dokeza Platfrom.
Hypothes.is a very developed fork and replacement of AnnotatorJS - `Hypothe.is`_.

.. _W3C published: https://www.w3.org/TR/2017/NOTE-annotation-html-20170223/
.. _annotateit.org: http://annotateit.org/
.. _Hypothe.is: https://hypothes.is/

The Dokeza Platform
===================

The Dokeza platform will allow public participation  of the development of a legislative bill in a structured manner.

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django


Introduction
------------

The Constitution of Kenya 2010 states:

* Sovereign power belongs to the People of Kenya and they may exercise it either directly or through their democratically elected representatives; 
* the right of access to information and 
* the right of public participation in governance processes. 

Context
^^^^^^^
Mzalendo keeps an eye on Kenyan Parliamentary business and documents each
Parliamentarians contribution as captured in the official Hansard. We also provide
Parliamentarians biographical information, contacts and details of their Constituency Development Fund allocation.

Mzalendo's mission heavily rests on three principles of the sovereign power belonging to the People of Kenya and they may exercise it either directly or through their democratically elected representatives;

* the right of access to information and the right of public participation in governance processes.

Although, **Article 118** of the Constitution expects Parliament to facilitate public
participation and involvement in the legislative and other business of Parliament and its committees, this is often not the case. 

Platform
^^^^^^^^
On the platform, a legislative bill would be open to all and would be available and optimized to be read on any smart device of choice.
Through the process of annotating and commenting on a bill, Mzalendo would draw attention to the bill, bring communities of interest together, include specialist opinion on the subject and allow the public to voice their opinion.

To contribute an opinion or comment, a user would be required to register online. The platform will allow curation that will allow:

* 'Trend' the bills that are receiving the most attention
* Show choice comments made on the Bills
* Share annotations and comments on Social Media

The Dokeza Platform will enhance *Public Participation* as well as *Access to Information*. The platform will generate user informed memoranda from the annotations and this will be compared to actual Bill.
The memoranda will be presented with the bill for further Parliamentary committee deliberation, assent and the gazetting.

