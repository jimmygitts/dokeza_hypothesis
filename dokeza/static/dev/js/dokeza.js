/* Bills page tab switcher */


var allList = $('#all__list'),
  assemblyList = $('#assembly_list'),
  senateList = $('#senate__list'),
  allBills = $('all__bills'),
  assemblyBills = $('#assembly__bills'),
  senateBills = $('#senate__bills');

(function() {
  allBills.show();
})();

senateList.click(function() {
  allList.removeClass('active');
  assemblyList.removeClass('active');
  senateList.addClass('active');
  assemblyBills.hide();
  allBills.hide();
  senateBills.show();
});

assemblyList.click(function() {
  allList.removeClass('active');
  assemblyList.addClass('active');
  senateList.removeClass('active');
  assemblyBills.show();
  allBills.hide();
  senateBills.hide();
});
