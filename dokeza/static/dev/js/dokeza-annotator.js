(function() {
  var app = new annotator.App();
  app.include(annotator.ui.main, {
    element: document.querySelector('#content')
  });
  app.include(annotator.identity.simple);
  app.include(annotator.storage.http, {
    prefix: window.location.origin,
  });
  app.include(function() {
    return {
      beforeAnnotationCreated: function(annotation) {
        annotation.uri = window.location.href;
      }
    }
  });
  app.start().then(function() {
    app.annotations.load({
      uri: window.location.href
    });
  });
})();
