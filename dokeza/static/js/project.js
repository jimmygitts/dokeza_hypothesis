/* Project specific Javascript goes here. */

/*
 ---------- Bootstrap 4 options -------------
*/
// $('.form-group').removeClass('row');

/*
    ============ AnnotatorJS Settings ==============
*/

/*  ---------- Annotator v2.0 ---------------------- */

// var ann = new Annotator(document.body);
//     ann.start();

// function getCookie(name) {
//         var cookieValue = null;
//         if (document.cookie && document.cookie !== '') {
//             var cookies = document.cookie.split(';');
//             for (var i = 0; i < cookies.length; i++) {
//                 var cookie = jQuery.trim(cookies[i]);
//                 // Does this cookie string begin with the name we want?
//                 if (cookie.substring(0, name.length + 1) === (name + '=')) {
//                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
//                     break;
//                 }
//             }
//         }
//         return cookieValue;
//     }
// var csrftoken = getCookie('csrftoken');

/* The default Anotator app */

// var app = new annotator.App();
// app.include(annotator.ui.main);
// app.include(annotator.storage.http);
// app.start()


/* Dokeza Configured Annotator App */

var app = new annotator.App();
app.include(annotator.ui.main, {
  element: document.querySelector('#content')
});
app.include(annotator.identity.simple);
app.include(annotator.storage.http, {
  prefix: window.location.origin,
});
app.include(function() {
  return {
    beforeAnnotationCreated: function(annotation) {
      annotation.uri = window.location.href;
    }
  }
});
app.start().then(function() {
  app.annotations.load({
    uri: window.location.href
  });
});
// app.start().then(function () {
//     app.ident.identity = 'dokezamasta';
// });


/* =========== Comment Markdown Preview =================== */

//     $(".content-markdown").each(function(){
//             var content = $(this).text();
//             var markedContent = marked(content);
//             $(this).html(markedContent);
//     });
//     $(".post-detail-item img").each(function(){
//             $(this).addClass("img-responsive");
//     });

//     var contentInput = $("#id_content");

//     function setContent(value){
//         var markedContent = marked(value);
//         $("#preview-content").html(markedContent);
//         $("#preview-content img").each(function(){
//             $(this).addClass("img-responsive");
//         });
//     }
//     setContent(contentInput.val());

//     contentInput.keyup(function(){
//         var newContent = $(this).val();
//         setContent(newContent);
//     });

//     var titleInput = $("#id_title");

//     function setTitle(value) {
//         $("#preview-title").text(value);
//     }
//     setTitle(titleInput.val());

//     titleInput.keyup(function(){
//         var newContent = $(this).val();
//         setTitle(newContent);
//     });

//     $(".comment-reply-btn").click(function(event){
//         event.preventDefault();
//         $(this).parent().next(".comment-reply").fadeToggle();
//     });
// });
