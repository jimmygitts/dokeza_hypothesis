/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

(function () {
  var app = new annotator.App();
  app.include(annotator.ui.main, {
    element: document.querySelector('#content')
  });
  app.include(annotator.identity.simple);
  app.include(annotator.storage.http, {
    prefix: window.location.origin
  });
  app.include(function () {
    return {
      beforeAnnotationCreated: function beforeAnnotationCreated(annotation) {
        annotation.uri = window.location.href;
      }
    };
  });
  app.start().then(function () {
    app.annotations.load({
      uri: window.location.href
    });
  });
})();

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(0);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYTIwZWQ4MTliZThkNmQ1ZDU4NDM/OTA5NyoiLCJ3ZWJwYWNrOi8vLy4vZG9rZXphL3N0YXRpYy9kZXYvanMvZG9rZXphLWFubm90YXRvci5qcyJdLCJuYW1lcyI6WyJhcHAiLCJhbm5vdGF0b3IiLCJBcHAiLCJpbmNsdWRlIiwidWkiLCJtYWluIiwiZWxlbWVudCIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvciIsImlkZW50aXR5Iiwic2ltcGxlIiwic3RvcmFnZSIsImh0dHAiLCJwcmVmaXgiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsIm9yaWdpbiIsImJlZm9yZUFubm90YXRpb25DcmVhdGVkIiwiYW5ub3RhdGlvbiIsInVyaSIsImhyZWYiLCJzdGFydCIsInRoZW4iLCJhbm5vdGF0aW9ucyIsImxvYWQiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQSxDQUFDLFlBQVc7QUFDVixNQUFJQSxNQUFNLElBQUlDLFVBQVVDLEdBQWQsRUFBVjtBQUNBRixNQUFJRyxPQUFKLENBQVlGLFVBQVVHLEVBQVYsQ0FBYUMsSUFBekIsRUFBK0I7QUFDN0JDLGFBQVNDLFNBQVNDLGFBQVQsQ0FBdUIsVUFBdkI7QUFEb0IsR0FBL0I7QUFHQVIsTUFBSUcsT0FBSixDQUFZRixVQUFVUSxRQUFWLENBQW1CQyxNQUEvQjtBQUNBVixNQUFJRyxPQUFKLENBQVlGLFVBQVVVLE9BQVYsQ0FBa0JDLElBQTlCLEVBQW9DO0FBQ2xDQyxZQUFRQyxPQUFPQyxRQUFQLENBQWdCQztBQURVLEdBQXBDO0FBR0FoQixNQUFJRyxPQUFKLENBQVksWUFBVztBQUNyQixXQUFPO0FBQ0xjLCtCQUF5QixpQ0FBU0MsVUFBVCxFQUFxQjtBQUM1Q0EsbUJBQVdDLEdBQVgsR0FBaUJMLE9BQU9DLFFBQVAsQ0FBZ0JLLElBQWpDO0FBQ0Q7QUFISSxLQUFQO0FBS0QsR0FORDtBQU9BcEIsTUFBSXFCLEtBQUosR0FBWUMsSUFBWixDQUFpQixZQUFXO0FBQzFCdEIsUUFBSXVCLFdBQUosQ0FBZ0JDLElBQWhCLENBQXFCO0FBQ25CTCxXQUFLTCxPQUFPQyxRQUFQLENBQWdCSztBQURGLEtBQXJCO0FBR0QsR0FKRDtBQUtELENBckJELEkiLCJmaWxlIjoianMvZG9rZXphLWFubm90YXRvci5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKVxuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi4vXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYTIwZWQ4MTliZThkNmQ1ZDU4NDMiLCIoZnVuY3Rpb24oKSB7XG4gIHZhciBhcHAgPSBuZXcgYW5ub3RhdG9yLkFwcCgpO1xuICBhcHAuaW5jbHVkZShhbm5vdGF0b3IudWkubWFpbiwge1xuICAgIGVsZW1lbnQ6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjb250ZW50JylcbiAgfSk7XG4gIGFwcC5pbmNsdWRlKGFubm90YXRvci5pZGVudGl0eS5zaW1wbGUpO1xuICBhcHAuaW5jbHVkZShhbm5vdGF0b3Iuc3RvcmFnZS5odHRwLCB7XG4gICAgcHJlZml4OiB3aW5kb3cubG9jYXRpb24ub3JpZ2luLFxuICB9KTtcbiAgYXBwLmluY2x1ZGUoZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGJlZm9yZUFubm90YXRpb25DcmVhdGVkOiBmdW5jdGlvbihhbm5vdGF0aW9uKSB7XG4gICAgICAgIGFubm90YXRpb24udXJpID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XG4gICAgICB9XG4gICAgfVxuICB9KTtcbiAgYXBwLnN0YXJ0KCkudGhlbihmdW5jdGlvbigpIHtcbiAgICBhcHAuYW5ub3RhdGlvbnMubG9hZCh7XG4gICAgICB1cmk6IHdpbmRvdy5sb2NhdGlvbi5ocmVmXG4gICAgfSk7XG4gIH0pO1xufSkoKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL2Rva2V6YS9zdGF0aWMvZGV2L2pzL2Rva2V6YS1hbm5vdGF0b3IuanMiXSwic291cmNlUm9vdCI6IiJ9