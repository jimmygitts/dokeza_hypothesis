# -*- coding: utf-8 -*-
from django.conf import settings
from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter


class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def get_login_redirect_url(self, request):
        path = "/users/~profile_update/"
        return path.format(email=request.user.email)


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request, sociallogin):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def get_login_redirect_url(self, request):
        path = "/users/~profile_update/"
        return path.format(email=request.user.email)


class DokezaAccountAdapter(DefaultAccountAdapter):
    def get_login_redirect_url(self, request):
        path = "/users/~profile_update/"
        # path = "/users/{email}/"
        return path.format(email=request.user.email)
