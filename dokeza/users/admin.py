# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from .models import User, Profile, Institution


class InstitutionAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'gender', 'bio', 'is_editor',
                    'is_member_of_parliament')


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name = 'profile'


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This email that is your username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        email = self.cleaned_data["email"]
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_username'])

class MyUserAdmin(admin.ModelAdmin):
    """
    Our user model extends 'AbstractBaseUser', so we need to define a custom ModelAdmin class. It may be possible to subclass the default django.contrib.auth.admin.UserAdmin; however, we'll need to override any of the definitions that refer to fields on 'django.contrib.auth.models.AbstractUser' that aren’t on your custom user class.

    Ours extends 'AbstractBaseUser'.

    https://docs.djangoproject.com/en/dev/topics/auth/customizing/#custom-users-and-django-contrib-admin
    """

    form = MyUserChangeForm
    add_form = MyUserCreationForm
    readonly_fields = ("date_joined",)
    inlines = (ProfileInline, )
    exclude = ('username',)

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(MyUserAdmin, self).get_inline_instances(request, obj)

    list_display = ('email', 'first_name', 'last_name', 'is_superuser')
    search_fields = ['first_name', 'last_name']

    fieldsets = (
        (None, {'fields': ('email', 'password',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff',)}),
        (_('Important dates'), {'fields': ('date_joined', 'last_login',)}),
        (_('Groups'), {'fields': ('groups', 'user_permissions')}),
    )


admin.site.register(User, MyUserAdmin)

# @admin.register(User)
# class MyUserAdmin(AuthUserAdmin):
#     list_display = ('email', 'is_superuser')
#     search_fields = ['email']

#     form = MyUserChangeForm
#     add_form = MyUserCreationForm
#     fieldsets = AuthUserAdmin.fieldsets
#     inlines = (ProfileInline, )


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Institution, InstitutionAdmin)
