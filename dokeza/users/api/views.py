import datetime
from django.db.models import Q
from django.conf import settings
from django.contrib.auth import get_user_model
import jwt
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)

from rest_framework.generics import (
    ListAPIView, CreateAPIView, RetrieveAPIView,
    DestroyAPIView,
)

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_payload_handler
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from .serializers import UserCreateSerializer, UserLoginSerializer
    # UserDetailSerializer,
    # UserListSerializer,
    # VisitorSerializer

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER

CONSUMER_KEY = 'i-want-to-quote-7TfWi4aJ3KkfZg2tNsMEDhqd'
CONSUMER_SECRET = settings.SECRET_KEY
CONSUMER_TTL = 86400

User = get_user_model()


class UserCreateAPIView(CreateAPIView):
    serializer_class = UserCreateSerializer
    permission_classes = [AllowAny]
    queryset = User.objects.all()


class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class GetJSONWebToken(APIView):
    """
    GetJSONWebToken is called and accepts the request.user, validates the user login credentials and sends back a JSON with the 'token'. If the user is Anonymous, the massage is given to login or register.
    """
    permission_classes = [IsAuthenticated]
    # serializer_class = JSONWebTokenSerializer

    def get(self, request, *args, **kwargs):
        user_obj = request.user
        if user_obj.is_authenticated():
            qs = User.objects.filter(username=user_obj.username)
            username = qs.first().username
            token = jwt.encode({
                'consumerKey': CONSUMER_KEY,
                'userId': username,
                'issuedAt': _now().isoformat() + 'Z',
                'ttl': CONSUMER_TTL
            }, CONSUMER_SECRET)
            return Response(token)
        else:
            return Response('Please login if you have registered and register if you have not.')


def _now():
    return datetime.datetime.utcnow().replace(microsecond=0)


get_jwt_token = GetJSONWebToken.as_view()
