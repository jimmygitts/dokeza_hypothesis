from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field
from crispy_forms.bootstrap import AppendedText, FormActions

from .models import User, Profile, Institution
from .boundaries import KENYAN_CONSTITUENCIES, KENYAN_COUNTIES


# These ModelForms can be strung together on the Registration page.
class UserForm(ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email',)


# class ProfileForm(ModelForm):

#     def __init__(self, *args, **kwargs):
#         super(ProfileForm, self).__init__(*args, **kwargs)
#         self.fields['bio'].widget.attrs.update({
#                 "placeholder": "Please enter a short bio here and set the 'County of Residence' and 'County of Interest' to allow filtering bills and other content.", })

#     class Meta:
#         model = Profile
#         fields = ('gender', 'institution', 'bio', 'county_residence', 'county_interest')


class InstitutionForm(ModelForm):
    class Meta:
        model = Institution
        fields = ('name',)


class SignupForm(forms.Form):
    GENDER_CHOICES = (
        (1, 'Male'),
        (2, 'Female'),
    )

    first_name = forms.CharField(max_length=30, label='First name')
    last_name = forms.CharField(max_length=30, label='Last name')
    # gender = forms.ChoiceField(widget=forms.Select, choices=GENDER_CHOICES)
    # county_interest = forms.ChoiceField(
    #     widget=forms.Select,
    #     choices=KENYAN_COUNTIES, required=False)
    # county_residence = forms.ChoiceField(
    #     widget=forms.Select,
    #     choices=KENYAN_COUNTIES, required=True)

    def signup(self, request, user):
        # Implement form.is_valid() here and save User first. This will require
        # a Profile entry view. Once complete a email verification.
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.save()

        # # Save your profile
        # user.profile = Profile()
        # user.profile.user_id = user.id
        # user.profile.save()

