# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from bills.views import BillDraftView
from posts.views import PetitionDraftView
from . import views


urlpatterns = [
    # URL pattern for the UserListView
    url(r'^$', views.UserListView.as_view(), name='list'),

    # URL pattern for the UserRedirectView
    url(r'^~redirect/$', views.UserRedirectView.as_view(), name='redirect'),

    # URL pattern for the UserDetailView
    url(r'^(?P<email>[\w.@+-]+)/$', views.UserDetailView.as_view(), name='detail'),

    # URL pattern for the UserUpdateView
    url(r'^~update/$', views.UserUpdateView.as_view(), name='update'),

    # URL pattern for the UserProfileUpdateView
    url(r'^~profile_update/$', views.ProfileUpdateView.as_view(), name='profile_update'),
    url(r'^~annotations/$', views.UserAnnotationView.as_view(), name='annotations'),
    url(r'^~comments/$', views.UserCommentsView.as_view(), name='comments'),
    url(r'^~draft-bill/$', BillDraftView.as_view(), name='draft-bill'),
    url(r'^~draft-petition/$', PetitionDraftView.as_view(), name='draft-petition'),
]
