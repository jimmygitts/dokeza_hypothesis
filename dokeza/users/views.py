from __future__ import absolute_import, unicode_literals

import datetime
import jwt

from django.conf import settings
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, RedirectView, TemplateView, UpdateView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import ModelFormMixin
from django.contrib.auth.mixins import LoginRequiredMixin

from django.utils.translation import ugettext, ugettext_lazy as _

from allauth.account.views import LoginView, SignupView
from .models import User, Profile



class UserListView(LoginRequiredMixin, ListView):
    model = User
    slug_field = "username"
    slug_url_kwarg = "username"
    # These next two lines tell the view to index lookups by username

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        users = User.objects.all().select_related('profile')
        context['users'] = users
        context['page'] = 'users'
        return context


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by email
    slug_field = "email"
    slug_url_kwarg = "email"

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        # follows = Follow.objects.get_follows().count()
        # context['follows'] = follows
        context['page'] = 'users'
        context['stingo'] = 'profile'
        return context


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail",
                       kwargs={"email": self.request.user.email})


class UserUpdateView(LoginRequiredMixin, UpdateView, ModelFormMixin):
    model = User
    fields = ['first_name', 'last_name', 'email']

    def get_context_data(self, **kwargs):
        context = super(UserUpdateView, self).get_context_data(**kwargs)
        context['page'] = 'users'
        context['stingo'] = 'profile'
        return context

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse("users:detail",
                       kwargs={"email": self.request.user.email})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(email=self.request.user.email)


class ProfileObjectMixin(SingleObjectMixin):
    """
    Provides views with the current user's profile.
    """
    model = Profile
    fields = ['gender', 'bio', 'county_residence', 'county_interest', 'facebook', 'twitter', 'view_contacts', 'country_code', 'mobile',]

    def get_context_data(self, **kwargs):
        context = super(ProfileObjectMixin, self).get_context_data(**kwargs)
        context['page'] = 'users'
        context['stingo'] = 'profile'
        return context

    def get_object(self):
        """Return's the current users profile."""
        try:
            return self.request.user.get_profile()
        except Profile.DoesNotExist:
            raise ValueError(
                "The user does not have an associated profile.")

    def get_success_url(self):
        return reverse("users:detail",
                       kwargs={"email": self.request.user.email})

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """Ensures that only authenticated users can access the view."""
        auth_user = ProfileObjectMixin
        return super(auth_user, self).dispatch(request, *args, **kwargs)


class ProfileUpdateView(ProfileObjectMixin, UpdateView):
    """
    A view that displays a form for editing a user's profile.

    Uses a form dynamically created for the `Profile` model and
    the default model's update template.
    """
    def get_success_url(self):
        return reverse("users:detail",
                       kwargs={"email": self.request.user.email})


# AUTH TOKEN - This will only be useful if the Annotator client
# and the DOKEZA pay attention to these keys.
def send_token(request):
    print(request)
    print(dir(request))
    user = request.user
    user_id = user.username

    if request.method == "GET" and user.is_authenticated():
        return jwt.encode({
            'consumerKey': settings.SECRET_KEY,
            'userId': user_id,
            'issuedAt': _now().isoformat() + 'Z',
            'ttl': 86400
        }, settings.PROVIDER_SECRET_KEY)
    else:
        return HttpResponse('Please log in to annotate the document!')


def _now():
    return datetime.datetime.utcnow().replace(microsecond=0)

# ---------------------------------------------------------
# User Panels Views
# ---------------------------------------------------------

class UserAnnotationView(TemplateView):
    template_name = 'users/user_annotations_new.html'

    def get_context_data(self, **kwargs):
        context = super(UserAnnotationView, self).get_context_data(**kwargs)
        context['page'] = 'users'
        context['stingo'] = 'annotations'
        return context

class UserCommentsView(TemplateView):
    template_name = 'users/user_comments.html'

    def get_context_data(self, **kwargs):
        context = super(UserCommentsView, self).get_context_data(**kwargs)
        context['page'] = 'users'
        context['stingo'] = 'comments'
        return context


        
