#!/usr/bin/env python
# encoding: utf-8
"""
cal/models.py

Created by Jimmy Gitonga on 2016-10-27.
Copyright (c) 2016 Afroshok. All rights reserved.
"""
# from django.db import models
# from django.utils.translation import ugettext_lazy as _
# from django.db.models import permalink


# class Category(models.Model):
#     """Category model."""
#     title = models.CharField(_('title'), max_length=100)
#     slug = models.SlugField(_('slug'), unique=True)

#     class Meta:
#         verbose_name = _('category')
#         verbose_name_plural = _('categories')
#         db_table = 'categories'
#         ordering = ('title',)

#     def __unicode__(self):
#         return u'%s' % self.title

#     @permalink
#     def get_absolute_url(self):
#         return ('category_detail', None, {'slug': self.slug})
