"""
cal/views.py

Created by Jimmy Gitonga on 2016-10-27.
Copyright (c) 2016 Afroshok. All rights reserved.
"""

import time
from datetime import date, datetime, timedelta
import calendar

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.views.generic import TemplateView
from django.forms.models import modelformset_factory

from public_participation.models import PublicEvent

mnames = "January February March April May June July August September October November December"
mnames = mnames.split()


class CalYearView(TemplateView):
    template_name = 'cal/year.html'

    def get_context_data(self, year):
        """Main listing, years and months; three years per page."""
        # prev / next years
        if year:
            year = int(year)
        else:
            year = time.localtime()[0]
        nowy, nowm = time.localtime()[:2]
        lst = []

        # create a list of months for each year, indicating ones that contain events and current
        for y in [year, year + 1, year + 2]:
            mlst = []
            for n, month in enumerate(mnames):

                event = current = False   # are there Event(s) for this month; current month?
                events = PublicEvent.objects.filter(start__year=y,
                                              start__month=n + 1)

                if events:
                    event = True
                if y == nowy and n + 1 == nowm:
                    current = True

                mlst.append(dict(n=n + 1, name=month, event=event, current=current))
            lst.append((y, mlst))

        return dict(years=lst, year=year, page='public', stingo='events')


class CalMonthView(TemplateView):
    template_name = 'cal/month.html'

    def get_context_data(self, year, month, change=None):
        """Listing of days in `month`."""

        year, month = int(year), int(month)

        # apply next / previous change
        if change in ("next", "prev"):
            now, mdelta = date(year, month, 1), timedelta(days=30)
            if change == "next":
                mod = mdelta
            elif change == "prev":
                mod = -mdelta

            year, month = (now + mod).timetuple()[:2]

        # init variables
        cal = calendar.Calendar()
        month_days = cal.itermonthdays(year, month)
        nyear, nmonth, nday = time.localtime()[:3]
        lst = [[]]
        week = 0

        # make month lists containing list of days for each week
        # each day tuple will contain list of events and 'current' indicator
        for day in month_days:
            events = current = False   # are there events for this day; current day?
            if day:
                events = PublicEvent.objects.filter(start__year=year, start__month=month, start__day=day)
                if day == nday and year == nyear and month == nmonth:
                    current = True

            lst[week].append((day, events, current))
            if len(lst[week]) == 7:
                lst.append([])
                week += 1

        return dict(year=year, month=month, month_days=lst, page='public', stingo='events', mname=mnames[month - 1])
