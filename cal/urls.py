#!/usr/bin/env python
# encoding: utf-8
"""
cal/urls.py

Created by Jimmy Gitonga on 2010-12-28.
Copyright (c) 2010 __MyCompanyName__. All rights reserved.
"""
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^(?P<year>\d{4})/$', CalYearView.as_view(), name='year'),
    url(r'^month/$', CalMonthView.as_view(), name='month'),
    url(r'^month/(?P<year>\d{4})/(?P<month>\d+)/$',
        CalMonthView.as_view(), name='month'),
    url(r'^month/(?P<year>\d{4})/(?P<month>\d+)/(?P<change>\w{4})/$', CalMonthView.as_view(), name='month'),
]
