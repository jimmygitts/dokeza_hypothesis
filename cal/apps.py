from django.apps import AppConfig


class CommentsConfig(AppConfig):
    name = 'cal'
    verbose_name = 'Kalenda'
