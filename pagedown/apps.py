from django.apps import AppConfig


class CommentsConfig(AppConfig):
    name = 'pagedown'
    verbose_name = "Pagedown"
