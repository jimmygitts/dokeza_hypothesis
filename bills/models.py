# import os
from ckeditor.fields import RichTextField
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.core.urlresolvers import reverse
from django.db import models
# from django.db.models.signals import post_save
# from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from annotation.models import Annotation
from comments.models import Comment

# from PyPDF2 import PdfFileReader
# from wand.image import Image

from taggit.managers import TaggableManager
from hitcount.models import HitCount, HitCountMixin


class Bill(HitCountMixin, models.Model):
    """
    This is the legislative draft bill.

    Description:
    Kenya has two houses in the Parliament, the National Assembly and the Senate. Both houses are sources of draft bills that the Kenyan public are expected to engage with during the Public Participation period.

    The draft bill on this platform has an owner. That could be a member of
    Parliament, or an institutional representative or a member of the public.

    In one case, the owner would like to develop the draft bill and later open
    it up privately to stakeholders and engage with them before the draft bill
    is presented to a house.

    In the second case, a draft bill has been presented to a house of
    Parliament for the first reading and is then put o nthis platform to open
    it up to Public Participation. This bill instance would be owned by the
    Dokeza Platform administrator, in this case, Mzalendo Trust.
    """

    BILL_TYPE = (
        (1, 'National Assembly'),
        (2, 'Senate'),
    )

    SPONSOR_TITLE = (
        (1, 'Member of the National Assembly'),
        (2, 'Senator'),
        (3, 'Leader of Majority Party'),
        (4, 'Leader of Minority Party.'),
        (5, 'Senate Majority Leader'),
        (6, 'Senate Majority Whip'),
        (5, 'Chairperson, Justice and Legal Affairs Committee'),
        (6, 'Chairperson, Committee on Finance, Planning and Trade'),
        (7, 'Chairperson, Budget and Appropriations Committee'),
        (8, 'Chairperson, Committee on Finance, Commerce and Budget'),
        (9, 'Chairman, Administration and National Security'),
        (10, 'Chairperson, Committee on Information and Technology'),
        (11, 'Chairperson, Committee on Education, Research and Technology'),
        (12, 'Chairperson, National Assembly Committee on Health'),
        (13, 'Chairperson, Constituency Development Fund Committee'),
        (14, 'Chairperson, Standing Committee on Energy'),
        (15, 'Chairman, Standing Committee on Health'),
        (16, 'Chairman, Standing Committee on Education'),
        (17, 'Chairperson, Standing Committee on Legal Affairs and Human Rights'),
        (18, 'Chairman, Standing Committee on Labour and Social Welfare'),
        (19, 'Chairman, Sessional Committee on Delegated Legislation'),
        (20, 'Chairman, Senate Ad Hoc Committee on Legislation on Harambee (Voluntary Contribution)'),
        (21, 'Chairperson, Select Committee on Legislation on Royalties Accruing from Natural Resources in the Counties'),
        (22, 'Vice Chairperson, Committee on Information and Technology'),
        (23, 'Member, Committee on Legal Affairs and Human Rights'),
        (24, 'Member, Committee on Justice and Legal Affairs'),
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    bill_from = models.PositiveSmallIntegerField(
        _('Bill from'), choices=BILL_TYPE, default=1, blank=True, null=True)
    title = models.CharField(max_length=100, default='A bill')
    slug = models.SlugField(unique=True)
    purpose = models.TextField(max_length=500, blank=True, null=True)
    sponsor = models.CharField(max_length=500, blank=True, null=True)
    sponsor_title = models.PositiveSmallIntegerField(_('Sponsor Title'), choices=SPONSOR_TITLE, default=1, blank=True, null=True)
    body = RichTextField(
        blank=True,
        help_text="Copy the Bill and paste with all its styling. If necessary use the editing tools to style it sufficiently.")
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    pdf = models.FileField(upload_to='bills/', blank=True, null=True, help_text='Upload the actual bill here.')
    bill_pic = models.ImageField(upload_to='bill_pics/', blank=True, null=True,
                                 help_text='Upload an image of the top stamped page of the bill.')
    private = models.BooleanField(default=True)
    first_reading = models.DateTimeField(auto_now_add=False,
                                         blank=True, null=True)
    second_reading = models.DateTimeField(auto_now_add=False,
                                          blank=True, null=True)
    third_reading = models.DateTimeField(auto_now_add=False,
                                         blank=True, null=True)
    assented_to = models.BooleanField(default=False)
    assented_date = models.DateTimeField(auto_now_add=False,
                                         blank=True, null=True)
    updated_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    law_reference = models.URLField(blank=True, null=True, help_text="This should be a link to the Kenya Law Review repository of the bill.")
    tags = TaggableManager()
    hit_count_generic = GenericRelation(
        HitCount, object_id_field='object_pk',
        related_query_name='hit_count_generic_relation')

    @property
    def annotations(self):
        instance = self
        qs = Annotation.objects.filter_by_instance(instance)
        return qs

    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(instance)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type

    def get_absolute_url(self):
        return reverse('bills:detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    # ----- This is a implementation of auto-pdf-2-image. It works on the
    # CLI but was problematic to implement in Docker
    # ----------------------------------------------
    # def convert_to_png(self, width=0, height=0):
    #     self.__convert_to_img__(width, height, 'png')

    # def convert_to_jpg(self, width=0, height=0):
    #     self.__convert_to_img__(width, height, 'jpg')

    # def __convert_to_img__(self, width, height, format='jpg'):
    #     size = ''
    #     if width and height:
    #         size = '_' + str(width) + 'x' + str(height) + 'px'

    #     # ---- Where to find the pdf ----
    #     filename = self.pdf.name
    #     filepath = settings.MEDIA_ROOT + '/' + filename
    #     # ----- Where to place the images ------
    #     output_dir = filepath + '_' + format + size + '/'
    #     os.mkdir(output_dir)
    #     # if filename is True:
    #     # else:
    #     #     return ValueError("Please add a pdf to use")
    #     # ---- Activate to read all the pages in the PDF file ----
    #     # input_file = PdfFileReader(file(filepath, 'rb'))
    #     for i in range(3):
    #         with Image(filename=filepath + '[' + str(i) + ']', resolution=300) as img:
    #             if len(size) > 0:
    #                 img.resize(width, height, filter='gaussian')
    #             img.format = format
    #             img.save(filename=output_dir + str(i) + '.' + format)

# --------- Receiver for the Save PDF signal ----------------
# @receiver(post_save, sender=Bill)
# def convert_to_img(sender, instance, **kwargs):
#     print('I have saved')
#     instance.convert_to_png(630, 891)
