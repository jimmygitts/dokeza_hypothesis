from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render
from django.views import View
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.views.generic.detail import SingleObjectMixin
from bs4 import BeautifulSoup

from .forms import BillForm
from .models import Bill
from comments.forms import CommentForm
from comments.models import Comment


class BillListView(ListView):
    """
    The Bill List view lists the all bills and paginates after a certain number is displayed.
    """
    model = Bill
    template_name = 'bills/bills_list.html'

    def get_context_data(self, **kwargs):
        context = super(BillListView, self).get_context_data(**kwargs)
        while Bill.objects.all() is not None:
            bills = Bill.objects.all()
            context['bills'] = bills
            context['page'] = 'bills'
            context['stingo'] = 'all'
            return context


class AssemblyBillListView(ListView):
    """
    This is the National Assembly Bill List view. it lists the bills only specific to the National Assembly.
    """
    model = Bill
    template_name = 'bills/bills_national-assembly.html'

    def get_context_data(self, **kwargs):
        context = super(AssemblyBillListView, self).get_context_data(**kwargs)
        while Bill.objects.all() is not None:
            assembly_bills = Bill.objects.all().filter(bill_from=1)
            context['assembly_bills'] = assembly_bills
            context['page'] = 'bills'
            context['stingo'] = 'assembly'
            return context


class SenateBillListView(ListView):
    """
    This is the Senate Bill List view. it lists the bills only specific to the Senate.
    """
    model = Bill
    template_name = 'bills/bills_senate.html'

    def get_context_data(self, **kwargs):
        context = super(SenateBillListView, self).get_context_data(**kwargs)
        while Bill.objects.all() is not None:
            senate_bills = Bill.objects.all().filter(bill_from=2)
            context['senate_bills'] = senate_bills
            context['page'] = 'bills'
            context['stingo'] = 'senate'
            return context


class BillDetailView(DetailView):
    """
    This is the view shows the details related to a particular view. 
    """
    model = Bill

    def get_context_data(self, *args, **kwargs):
        context = super(BillDisplayView, self).get_context_data(
            *args, **kwargs)
        comments = self.object.comments

        initial_data = {
            "content_type": self.object.get_content_type,
            "object_id": self.object.id,
        }
        if self.object.bill_from == 1:
            house = 'assembly'
        else:
            house = 'senate'

        content = self.object.body
        soup = BeautifulSoup(content, 'html.parser')
        anchors = soup.find_all(lambda tag: tag.name == "a" and len(tag.attrs) == 2)
        toc = []
        for anchor in anchors:
            href = '<a href="#' + anchor.attrs['id'] + '">' + anchor.attrs['name'] + '</a>'
            toc.append('<li>' + href + '</li>')

        context["page"] = 'bills'
        context["stingo"] = house
        context["toc"] = toc
        context["comment_form"] = CommentForm(initial=initial_data)
        context["comments"] = comments

        return context


class BillCommentView(SingleObjectMixin, FormView):
    """
    This view embedes the Comment form below the bill details.
    """
    model = Bill
    form_class = CommentForm
    template_name = 'bills/bill_detail.html'

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()

        form = CommentForm(request.POST or None)
        if form.is_valid():
            c_type = form.cleaned_data.get("content_type")
            content_type = ContentType.objects.get(model=c_type)
            obj_id = form.cleaned_data.get('object_id')
            content_data = form.cleaned_data.get("content")
            parent_obj = None
            try:
                parent_id = int(request.POST.get("parent_id"))
            except:
                parent_id = None

            if parent_id:
                parent_qs = Comment.objects.filter(id=parent_id)
                if parent_qs.exists() and parent_qs.count() == 1:
                    parent_obj = parent_qs.first()

            new_comment, created = Comment.objects.get_or_create(
                user=request.user,
                content_type=content_type,
                object_id=obj_id,
                content=content_data,
                parent=parent_obj,
            )
            return HttpResponseRedirect(
                new_comment.content_object.get_absolute_url()
            )
        else:
            return self.form_invalid(form)


class BillDisplayView(View):
    """
    This view combines the Detail "GET" view and the Comment "POST" view.
    """
    def get(self, request, *args, **kwargs):
        view = BillDetailView.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = BillCommentView.as_view()
        return view(request, *args, **kwargs)


class BillDraftView(TemplateView):
    """
    This view presents an initial blank page that is used for developing the draft bill.
    """
    template_name = 'users/user_draftabill.html'

    def get_context_data(self, **kwargs):
        context = super(BillDraftView, self).get_context_data(**kwargs)
        # if request.user.is_authenticated():
        #     user_bills = Bill.objects.filter(owner=request.user)
        #     return user_bills
        # context['bills'] = user_bills
        context['page'] = 'users'
        context['stingo'] = 'draft_bill'
        return context

# class BillDraftView(SingleObjectTemplateResponseMixin, ModelFormMixin,
#         ProcessFormView):

#     def get_object(self, queryset=None):
#         try:
#             return super(BillDraftView,self).get_object(queryset)
#         except AttributeError:
#             return None

#     def get(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         return super(BillDraftView, self).get(request, *args, **kwargs)

#     def post(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         return super(BillDraftView, self).post(request, *args, **kwargs)
