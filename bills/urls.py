from django.conf.urls import url

from .views import (
    BillListView,
    AssemblyBillListView,
    SenateBillListView,
    BillDisplayView,
)

urlpatterns = [
    url(r'^$', BillListView.as_view(), name='list'),
    url(r'^assembly/$', AssemblyBillListView.as_view(), name='assembly'),
    url(r'^senate/$', SenateBillListView.as_view(), name='senate'),
    url(r'^(?P<slug>[\w-]+)/$', BillDisplayView.as_view(), name='detail'),
]
