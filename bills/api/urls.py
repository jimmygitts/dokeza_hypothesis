from django.conf.urls import url

from .views import (
    BillCreateAPIView,
    BillDeleteAPIView,
    BillDetailAPIView,
    BillListAPIView,
    BillUpdateAPIView,
)

urlpatterns = [
    url(r'^$', BillListAPIView.as_view(), name='list'),
    url(r'^create/$', BillCreateAPIView.as_view(), name='create'),
    url(r'^(?P<slug>[\w-]+)/$', BillDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<slug>[\w-]+)/edit/$', BillUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<slug>[\w-]+)/delete/$', BillDeleteAPIView.as_view(), name='delete'),
]
  