# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-20 14:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bills', '0003_auto_20170315_1435'),
    ]

    operations = [
        migrations.AddField(
            model_name='bill',
            name='bill_pic',
            field=models.ImageField(blank=True, null=True, upload_to='bill_pics/'),
        ),
    ]
