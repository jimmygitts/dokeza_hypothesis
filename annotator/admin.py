from django.contrib import admin
from annotator.models import Annotation


class AnnotationAdmin(admin.ModelAdmin):
    list_display = ["id", "guid", "bill", "created", "updated"]
    readonly_field = ('guid', )


admin.site.register(Annotation, AnnotationAdmin)
