import uuid
import json

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models

from bills.models import Bill
from comments.models import Comment
from taggit.managers import TaggableManager


class Annotation(models.Model):
    """
    The annotation is attached to a particular bill. The annotation is made by
    a particular user. We have indexed both the bill abd the owner since these
    will be important search categories when compiling them into memoranda.
    """

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    bill = models.ForeignKey(Bill, db_index=True, related_name="annotations", blank=True, null=True, default=1)
    guid = models.CharField(max_length=64, unique=True, editable=False)
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    updated = models.DateTimeField(auto_now=True, db_index=True)
    data = models.TextField()  # all other annotation data as JSON
    tags = TaggableManager()

    def set_guid(self):
        self.guid = str(uuid.uuid4())

    def can_edit(self, user):
        if self.owner and self.owner != user and (
                not user or not user.has_perm('annotator.change_annotation')):
            return False
        return True

    def as_json(self, user=None):
        d = {
            "guid": self.guid,
            "bill": self.bill_id,
            "created": self.created.isoformat(),
            "updated": self.updated.isoformat(),
            "readonly": not self.can_edit(user),
        }

        d.update(json.loads(self.data))

        return d

    def update_from_json(self, new_data):
        d = json.loads(self.data)

        for k, v in new_data.items():
            # Skip special fields that we maintain and are not editable.
            if k in ('bill', 'guid', 'created', 'updated', 'readonly'):
                continue

            # Put other fields into the data object.
            d[k] = v

        self.data = json.dumps(d)

    def get_absolute_url(self):
        return reverse('annotator:detail', kwargs={'id': self.id})

    @staticmethod
    def as_list(qs=None, user=None):
        if qs is None:
            qs = Annotation.objects.all()
        return [
            obj.as_json(user=user)
            for obj in qs.order_by('-updated')
        ]

    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(instance)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type
