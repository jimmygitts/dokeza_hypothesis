from django.conf.urls import url

from .views import (
    AnnotationCreateAPIView,
    AnnotationDeleteAPIView,
    AnnotationDetailAPIView,
    AnnotationListAPIView,
    AnnotationUpdateAPIView,
)

urlpatterns = [
    url(r'^$', AnnotationListAPIView.as_view(), name='list'),
    url(r'^create/$', AnnotationCreateAPIView.as_view(), name='create'),
    url(r'^(?P<id>[\w-]+)/$', AnnotationDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<id>[\w-]+)/update/$', AnnotationUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<id>[\w-]+)/delete/$', AnnotationDeleteAPIView.as_view(), name='delete'),
]
  