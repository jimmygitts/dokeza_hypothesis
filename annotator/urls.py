from django.conf.urls import url

from annotator.views import Root, Index, Annot, Search, EditorView

urlpatterns = [
    url(r'^$', Root.as_view(), name='root'),
    url(r'^annotations/$', Index.as_view(), name='index'),
    url(r'^annotations/([\w\-]+)$', Annot.as_view(),
        name='annotation'),
    url(r'^search$', Search.as_view(), name='search'),

    # public pages
    url(r'^bill/(?P<slug>[\w-]+)/edit/$', EditorView.as_view(),
        name='editor'),
]
