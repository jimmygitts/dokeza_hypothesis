# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-08 18:40
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('hitcount', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='hit',
            name='user',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='hitcount',
            unique_together=set([('content_type', 'object_pk')]),
        ),
    ]
