import uuid
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.conf import settings
from django.db import models
from hitcount.models import HitCount, HitCountMixin


class Annotation(HitCountMixin, models.Model):
    """
    The Annotation application stores annotations bsed on the web pages they appear on and the highlighted text. Likes (hits) will be part of the 

    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    annotator_schema_version = models.CharField(max_length=8, default="v1.0")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    text = models.TextField()
    quote = models.TextField()
    # We can generate the bill slug for the uri 
    uri = models.CharField(max_length=4096, blank=False, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    consumer = models.CharField(max_length=64, default="Mzalendo Dokeza")
    hit_count_generic = GenericRelation(
        HitCount, object_id_field='object_pk',
        related_query_name='hit_count_generic_relation')

    class Meta:
        ordering = ('created',)


class Range(models.Model):
    start = models.CharField(max_length=128)
    end = models.CharField(max_length=128)
    startOffset = models.IntegerField()
    endOffset = models.IntegerField()
    annotation = models.ForeignKey(Annotation, related_name="ranges")
