from django.contrib import admin

from annotation.models import Annotation


class AnnotationAdmin(admin.ModelAdmin):
    list_display = ["user", "created", "updated"]
    exclude = ('annotator_schema_version',)


admin.site.register(Annotation, AnnotationAdmin)
