const {
  mix
} = require('laravel-mix');
let devPath = 'dokeza/static/dev/';
let distPath = 'dokeza/static/dist/';
let node_modules = 'node_modules/'
mix
  .setPublicPath('dokeza/static/dist/')
  .copy(node_modules + 'font-awesome/fonts/', distPath + 'fonts/')
  .copy(node_modules + 'jquery/dist/jquery.min.js', distPath + 'js/vendor/')
  .copy(node_modules + 'jquery/dist/jquery.min.map', distPath + 'js/vendor/')
  .combine([
    node_modules + 'tether/dist/js/tether.js',
    node_modules + 'bootstrap/dist/js/bootstrap.js',
    node_modules + 'ie10-viewport-bug-workaround.js/ie10-viewport-bug-workaround.js',
    node_modules + 'quill/dist/quill.js',
  ], distPath + 'js/vendor.js')
  .js(devPath + 'js/dokeza.js', 'js/')
  .js(devPath + 'js/dokeza-annotator.js', 'js/')
  .js(devPath + 'js/dokeza-quill.js', 'js/')
  .sass(devPath + 'scss/dokeza.scss', 'css/')
  .sourceMaps();
